package com.collector_agency.clients;

import com.collector_agency.banks.Bank;

import java.util.HashMap;
import java.util.Random;

public class Client {
    private String clientName;
    private int neededCredit;
    private int tookCredit;
    private int remainingCredit;

    private HashMap<Bank, Integer> userBanks = new HashMap<>();

    public Client(String clientName, int neededCredit, int tookCredit) {
        this.clientName = clientName;
        this.neededCredit = neededCredit;
        this.tookCredit = tookCredit;
        this.remainingCredit = neededCredit;
    }

    public String getClientName() {
        return clientName;
    }

    public void takeCredit(Bank bank, int tookCredit) {
        userBanks.put(bank, tookCredit);
        this.tookCredit += tookCredit;
        this.remainingCredit = this.neededCredit - this.tookCredit;
    }

    public int getRemainingCredit() {
        return remainingCredit;
    }

    private int getMonthPay() {
        return new Random().nextInt(500) + 500;
    }

    public void payCredit() {
        int amount = getMonthPay() / userBanks.size();
        System.out.printf("Client: %s. Pay %d.%n", this.clientName, amount);
        for (Bank bank : this.userBanks.keySet()) {

            if (this.userBanks.get(bank) - amount > 0) {
                this.userBanks.put(bank, this.userBanks.get(bank) - amount);
            } else {
                this.userBanks.put(bank, 0);
                System.out.printf("Client: %s. Repaid credit in bank: %s.%n", this.clientName, bank.getBankName());
            }
            System.out.printf("Client: %s. Remaining money %d in bank: %s.%n", this.clientName, this.userBanks.get(bank), bank.getBankName());
        }
    }

    public int getIndebtedness() {
        int count = 0;
        for (Bank bank : this.userBanks.keySet()) {
            count += this.userBanks.get(bank);
        }
        return count;
    }
}