package com.collector_agency;

import com.collector_agency.banks.Bank;
import com.collector_agency.clients.Client;
import com.collector_agency.collector.Collector;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        ArrayList<Bank> banks = createBanks();
        ArrayList<Client> clients = createClients();

        for (Client client : clients) {
            for (Bank bank : banks) {
                if (client.getRemainingCredit() > 0) {
                    if (!bank.isUserClient(client.getClientName())) {
                        System.out.printf("%s you are new client of bank: %s.%n", client.getClientName(), bank.getBankName());
                        if (client.getRemainingCredit() - bank.getCreditLimit() > 0) {
                            System.out.printf("Your remaining credit %d biggest as bank limit %d.%nGet all bank limit %d.%n",client.getRemainingCredit(),bank.getCreditLimit(), bank.getCreditLimit());
                            client.takeCredit(bank, bank.getCreditLimit());
                            bank.addNewUser(client.getClientName(), bank.getCreditLimit());
                        } else {
                            System.out.printf("Your remaining credit %d less as bank limit %d.%n", client.getRemainingCredit(), bank.getCreditLimit());
                            client.takeCredit(bank, client.getRemainingCredit());
                            bank.addNewUser(client.getClientName(), client.getRemainingCredit());
                        }
                        System.out.printf("Your credit limit is %d.%n", client.getRemainingCredit());
                        System.out.println("**********************************************************");
                    } else {
                        System.out.printf("You are client of bank %s. Please choose other bank.%n", bank.getBankName());
                    }
                } else {
                    break;
                }
            }
        }

        for (int i = 1; i <= 12; i++) {
            for (Client client : clients) {
                System.out.printf("Client pay %d month.%n", i);
                client.payCredit();
            }
                System.out.println("**********************************************************");
        }

        Collector collector = new Collector();
        for (Client client : clients) {
            int clientIndebtedness = client.getIndebtedness();
            if(clientIndebtedness>0){
            collector.setUsers(client.getClientName(), clientIndebtedness);
            }else collector.removeUsers(client.getClientName(), clientIndebtedness);
        }
        System.out.printf("List of debtors for collectors: %s", collector.toString());
    }

    private static ArrayList<Bank> createBanks() {
        ArrayList<Bank> banks = new ArrayList<>();
        banks.add(new Bank<>("Privat Bank", 3000));
        banks.add(new Bank<>("Alfa Bank", 5000));
        banks.add(new Bank<>("Ukrsotc Bank", 2000));
        banks.add(new Bank<>("Mega Bank", 1000));
        banks.add(new Bank<>("Delta Bank", 4000));
        return banks;
    }

    private static ArrayList<Client> createClients() {
        ArrayList<Client> clients = new ArrayList<>();
        clients.add(new Client("Misha", 12000, 0));
        clients.add(new Client("Anton", 7000, 0));
        clients.add(new Client("Sergey", 9000, 0));
        clients.add(new Client("Roma", 600, 0));
        clients.add(new Client("Dima", 8000, 0));
        return clients;
    }
}

